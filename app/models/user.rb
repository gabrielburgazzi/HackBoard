# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  email      :string
#  title      :integer
#  mobile     :string
#  password   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord
    enum title: [ :student, :graduated, :mentor ]

    has_many :groups_users
    has_many :posts
    has_many :comments
    has_many :user_post_claps
    has_many :user_post_bookmarks
    has_many :user_comment_claps

end
